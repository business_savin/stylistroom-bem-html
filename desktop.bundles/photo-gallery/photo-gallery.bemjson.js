module.exports = {
    block: 'page',
    title: 'Page photo-gallery',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: '/static/Magnific-Popup-master/dist/magnific-popup.css' },
        { elem: 'css', url: 'photo-gallery.min.css' }
    ],
    scripts: [
        { elem: 'js', url: 'https://code.jquery.com/jquery-1.11.1.min.js' },
        { elem: 'js', url: '/static/Magnific-Popup-master/dist/jquery.magnific-popup.min.js' },
        { elem: 'js', url: 'photo-gallery.min.js' }
    ],
    mods: { theme: 'islands' },
    content: [
        {
            block: 'header',
            js: true,
        },
        {
            block: 'content',
            content: [
                {
                    block: 'headline',
                    content: 'Фотогалерея'
                },
                {
                    block: 'tabs',
                    mods: {
                        theme: 'green',
                        type: 'horizontally'
                    },
                    js: true,
                    content: [
                        {
                            elem: 'navigation',
                            content: [
                                {
                                    elem: 'table',
                                    content: [
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-10'
                                            },
                                            mix: {
                                                block: 'tabs',
                                                elem: 'tab--active'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Услуги стилиста'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-20'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Ногтевой сервис'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-30'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Брови'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-40'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Макияж'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-50'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Ресницы'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-60'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Эпиляция & Шугаринг'
                                                }
                                            ]
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            block: 'tabs',
                            elem: 'wrap',
                            content: [
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-10',
                                    },
                                    mix:[
                                        {
                                            block: 'active'
                                        },
                                        {
                                            block: 'popup-gallery',
                                            js: true
                                        }
                                    ],
                                    content: [
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-1'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-1'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-2'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-2'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-3'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-3'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-4'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-4'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-5'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-5'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-6'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-6'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-7'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-7'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-8'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-8'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-9'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-9'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-10'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-10'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-11'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-11'
                                            }
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: 'https://source.unsplash.com/283x283/?-12'
                                            },
                                            content: {
                                                block: 'image',
                                                url: 'https://source.unsplash.com/283x283/?-12'
                                            }
                                        }
                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-20',
                                    },

                                    content: [
                                        {
                                            block: 'h2',
                                            content: 'Ногтевой сервис'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-30',
                                    },

                                    content: [
                                        {
                                            block: 'h2',
                                            content: 'Брови'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-40',
                                    },

                                    content: [
                                        {
                                            block: 'h2',
                                            content: 'Макияж'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-50',
                                    },

                                    content: [
                                        {
                                            block: 'h2',
                                            content: 'Ресницы'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-60',
                                    },

                                    content: [
                                        {
                                            block: 'h2',
                                            content: 'Эпиляция & Шугаринг'
                                        }

                                    ]
                                }
                            ]
                        }


                    ]
                },
                {
                    block: 'photo-gallery-page',
                    content:[
                        {
                            block: 'block',
                            mods:{
                                indent: 'top'
                            },
                            content: [
                                {
                                    block: 'pagination',
                                    content: [
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '1'
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '2'
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '3'
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '4'
                                        }

                                    ]
                                }
                            ]
                        }
                    ]
                },

            ]
        },
        {
            block: 'footer'
        }
    ]
};
