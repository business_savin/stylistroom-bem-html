module.exports = {
    block: 'page',
    title: 'Page services',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'services.min.css' }
    ],
    scripts: [
        { elem: 'js', url: 'https://code.jquery.com/jquery-1.11.1.min.js' },
        { elem: 'js', url: 'services.min.js' }
    ],
    mods: { theme: 'islands' },
    content: [
        {
            block: 'header',
            js: true
        },
        {
            block: 'content',
            content: [
                {
                    block: 'headline',
                    content: 'Услуги и цены'
                },
                {
                    block: 'tabs',
                    mods: {
                        type: 'row'
                    },
                    js: true,
                    content: [
                        {
                            elem: 'navigation',
                            content: [
                                {
                                    block: 'sidebar',
                                    js: 'true',
                                    sidebar: [
                                        {
                                            name: 'Услуги стилиста',
                                            url: '#',
                                            level: [
                                                {
                                                    name: 'Ногтевой сервис',
                                                    url: '#tab-10'
                                                },
                                                {
                                                    name: 'Ногтевой сервис Ногтевой сервис',
                                                    url: '#tab-20'
                                                },
                                                {
                                                    name: 'Ногтевой сервис',
                                                    url: '123'
                                                },

                                            ]
                                        },
                                        {
                                            name: 'Свитшоты',
                                            url: '#'
                                        },
                                        {
                                            name: 'Услуги визажиста',
                                            url: '#',
                                            level: [
                                                {
                                                    name: 'Мужчина',
                                                    url: '#tab-30',
                                                },
                                                {
                                                    name: 'Женщинам',
                                                    url: '#',
                                                }
                                            ]
                                        },
                                        {
                                            name: 'Депиляция',
                                            url: '#'
                                        },

                                    ]
                                },
                            ]
                        },
                        {
                            block: 'tabs',
                            elem: 'wrap',
                            content: [
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    elemMods:{
                                        indent:'top'
                                    },
                                    attrs: {
                                        id: 'tab-10',
                                    },
                                    mix:[
                                        {
                                            block: 'active'
                                        }
                                    ],
                                    content: [
                                        {
                                            block: 'block',
                                            mods:{
                                                border: 'none'
                                            },
                                            content: [
                                                {
                                                    block: 'row',
                                                    mods:{
                                                        type: 'text'
                                                    },
                                                    content: [
                                                        {
                                                            elem: 'col',
                                                            elemMods:{
                                                                mw: 8
                                                            },
                                                            content: [
                                                                {
                                                                    block: 'h2',
                                                                    content:' Цены'
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская от 5 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  700  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская до 10 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская до 15 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },

                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «Vidal Sassoon»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2300  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                
                                                            ]
                                                        },
                                                        {
                                                            elem: 'col',
                                                            elemMods:{
                                                                mw: 4
                                                            },
                                                            content: {
                                                                block: 'image',
                                                           
                                                                url:'/static/service.png'
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    elemMods:{
                                        indent:'top'
                                    },
                                    attrs: {
                                        id: 'tab-20',
                                    },
                                 
                                    content: [
                                        {
                                            block: 'block',
                                            mods:{
                                                border: 'none'
                                            },
                                            content: [
                                                {
                                                    block: 'row',
                                                    mods:{
                                                        type: 'text'
                                                    },
                                                    content: [
                                                        {
                                                            elem: 'col',
                                                            elemMods:{
                                                                mw: 8
                                                            },
                                                            content: [
                                                                {
                                                                    block: 'h2',
                                                                    content:' Цен2ы'
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская от 5 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  700  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская до 10 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская до 15 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «Vidal Sassoon»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2300  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                
                                                            ]
                                                        },
                                                        {
                                                            elem: 'col',
                                                            elemMods:{
                                                                mw: 4
                                                            },
                                                            content: {
                                                                block: 'image',
                                                           
                                                                url:'https://source.unsplash.com/571x732/?-fest'
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    elemMods:{
                                        indent:'top'
                                    },
                                    attrs: {
                                        id: 'tab-30',
                                    },
                                 
                                    content: [
                                        {
                                            block: 'block',
                                            mods:{
                                                border: 'none'
                                            },
                                            content: [
                                                {
                                                    block: 'row',
                                                    mods:{
                                                        type: 'text'
                                                    },
                                                    content: [
                                                        {
                                                            elem: 'col',
                                                            elemMods:{
                                                                mw: 8
                                                            },
                                                            content: [
                                                                {
                                                                    block: 'h2',
                                                                    content:' Цен5ы'
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская от 5 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  700  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская до 10 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка детская до 15 лет'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «Vidal Sassoon»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2300  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка мужская'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  1500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка под машинку'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  500  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка усов/бороды'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  600  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                                {tag: 'br'},
                                                                {
                                                                    block: 'row',
                                                                    content: [
                                                                        {
                                                                            block: 'text',
                                                                            content: 'Стрижка женская в технике «TONY & GUY»'
                                                                        },
                                                                        {
                                                                            block: 'text',
                                                                            content: [
                                                                                '— от',
                                                                                {
                                                                                    tag: 'b',
                                                                                    content: '  2000  '
                                                                                },  
                                                                                'рублей'
                                                                            ]
                                                                        }
                                                                    ]
                                                                },
                                                            ]
                                                        },
                                                        {
                                                            elem: 'col',
                                                            elemMods:{
                                                                mw: 4
                                                            },
                                                            content: {
                                                                block: 'image',
                                                           
                                                                url:'https://source.unsplash.com/571x732/?-row'
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                               
                            ]
                        }
                        
        
                    ]
                },
               
            ]
        },
        {
            block: 'modal-services',
            js: true,
            content: [
                {
                    block: 'link',
                    mods: {
                        pseudo: true,
                    },
                    mix:{
                        
                            block: 'button',
                            mods:{
                              theme: 'green',
                              size: 'xxl',
                              type: 'link'
                            },
                    },
                    content: 'ЗАПИСАТЬСЯ'
                },
                {
                    block: 'modal',
                    mods: {
                        autoclosable: true,
                        size: 'xl',
                        theme: 'islands'
                    },
                    content: [
                        {
                            block: 'row',
                           
                            content: [
                                {
                                    elem: 'col',
                                    elemMods:{
                                        mw:6
                                    },
                                    content: {
                                        block: 'image',
                                        url: '/static/service-2.png'
                                    }
                                },
                                {
                                    elem: 'col',
                                    mix:{
                                        block: 'modal',
                                        elem: 'form'
                                    },
                                    elemMods:{
                                        mw: 6
                                    },
                                    content:[
                                        {
                                            block: 'text',
                                            mods:{
                                                type: 'h1'
                                            },
                                            content: 'Заявка на обучение'
                                        },
                                        {
                                            block: 'text',
                                            mods:{
                                                type: 'h2'
                                            },
                                            content: 'курс по наращиванию ногтей (SHELLAC)'
                                        },
                                        {
                                            block: 'form-modal',
                                            tag: 'form',
                                            content:[
                                                {
                                                    block: 'input',
                                                    placeholder:'Имя'
                                                },
                                                {
                                                    block: 'input',
                                                    placeholder:'Номер'
                                                },
                                                {
                                                    block: 'input',
                                                    placeholder:'Email'
                                                },
                                                {
                                                    block: 'button',
                                                    mods:{
                                                        theme: 'green',
                                                        type: 'subbmit'
                                                    },
                                                    text: 'ЗАПИСАТЬСЯ НА КУРС'
                                                },
                                                {
                                                    block: 'text',
                                                    content: [
                                                        'Нажимая кнопку, вы даете согласие на ',
                                                        {
                                                            tag: 'br'
                                                        },
                                                        {
                                                            block: 'link',
                                                            url: '#',
                                                            content: 'обработку персональных данных'
                                                        }

                                                    ]
                                                }
                                            ]
                                        },
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        
        
        {
            block: 'footer'
        }
    ]
};
