module.exports = {
    block: 'page',
    title: 'Page reviews',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'reviews.min.css' }
    ],
    scripts: [
        { elem: 'js', url: 'https://code.jquery.com/jquery-1.11.1.min.js' },
        { elem: 'js', url: 'reviews.min.js' }
    ],
    mods: { theme: 'islands' },
    content: [
        {
            block: 'header',
            js: true,
            content: [
                'header content goes here'
            ]
        },
        {
            block: 'content',
            content: [
                {
                    block: 'headline',
                    content: 'Отзывы'
                },
                {
                    block: 'reviews-page',
                    content: [
                        {elem: 'wrap',
                        content: [
                            {
                                block: 'block',
                                mods:{
                                    indent: 'top'
                                },  
                                content: [
                                    {
                                        block: 'reviews',
                                        content: [
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-row'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-coll'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-blue'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-dick'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-row'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-coll'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-blue'
                                                    }
                                                }
                                            },
                                            {
                                                elem: 'cell',
                                                content: {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/283x283/?-dick'
                                                    }
                                                }
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]},
                        {
                            block: 'block',
                            mods:{
                                indent: 'top'
                            },
                            content: [
                                {
                                    block: 'pagination',
                                    content: [
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '1'
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '2'
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '3'
                                        },
                                        {
                                            elem: 'cell',
                                            tag: 'a',
                                            attrs:{
                                                href: '#'
                                            },
                                            content: '4'
                                        }
                                        
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            block: 'footer'
        }
    ]
};
