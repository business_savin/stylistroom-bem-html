module.exports = {
    block: 'page',
    title: 'Page employees',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },

        { elem: 'css', url: ' https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css' },
        { elem: 'css', url: '/static/Magnific-Popup-master/dist/magnific-popup.css' },
        { elem: 'css', url: 'employees.min.css' }
    ],
    scripts: [
        { elem: 'js', url: 'https://code.jquery.com/jquery-1.11.1.min.js' },
        { elem: 'js', url: 'https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js' },
        { elem: 'js', url: '/static/Magnific-Popup-master/dist/jquery.magnific-popup.min.js' },
        { elem: 'js', url: 'employees.min.js' }
    ],
    mods: { theme: 'islands' },
    content: [
        {
            block: 'header',
            js: true,
            content: [
                'header content goes here'
            ]
        },
        {
            block: 'content',
            content: [
                {
                    block: 'headline',
                    content: [
                        {
                            elem: 'h1',
                            content: 'СОТРУДНИКИ'
                        },
                        {
                            elem: 'text',
                            content: [
                                {
                                    tag: 'p',
                                    content: 'Мы очень серьёзно подходит к профессионализму мастеров в нашей студии. Именно поэтому, выбрав любого из наших сотрудников, Вы можете быть уверены, что результат превзойдет Ваши ожидания!'
                                },
                                {
                                    tag: 'p',
                                    content: 'В нашей студии работают только дипломированные мастера, которые проходили обучение в лучших школах и академиях. Средний опыт наших мастеров составляет более 4 лет. Наши мастера заинтересованы в качественной работе и сервисе.'
                                },
                                {
                                    tag: 'p',
                                    content: 'Мы заботимся о каждом клиенте  – и это не просто слова. Мы используем только оригинальные, проверенные в работе материалы, которые не вредят здоровью. '
                                }
                            ]
                        }
                    ]
                },
                {
                    block: 'tabs',
                    mods: {
                        theme: 'green',
                        type: 'horizontally'
                    },
                    js: true,
                    content: [
                        {
                            elem: 'navigation',
                            content: [
                                {
                                    elem: 'table',
                                    content: [
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-10'
                                            },
                                            mix: {
                                                block: 'tabs',
                                                elem: 'tab--active'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Ногтевой сервис'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-20'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Ресницы'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-30'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Эпиляция & Шугаринг'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-40'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Брови'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-50'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Cтилист'
                                                }
                                            ]
                                        },
                                    ]
                                }

                            ]
                        },
                        {
                            block: 'tabs',
                            elem: 'wrap',
                            content: [
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-10',
                                    },
                                    mix:[
                                        {
                                            block: 'active'
                                        }
                                    ],
                                    content: [
                                        {
                                            block: 'row',
                                            mods:{
                                                indent: 'none'
                                            },
                                            mix:{
                                                block: 'employees'
                                            },
                                            content: [
                                                {
                                                    elem: 'col',
                                                    elemMods:{
                                                        mw: 5,
                                                        border: true
                                                    },
                                                    content: [
                                                        {
                                                            block: 'employees-h1',
                                                            content:'Эпиляция & Шугаринг'
                                                        },
                                                        {
                                                            block: 'slider',
                                                            js: true,
                                                            content: [
                                                                {
                                                                    elem: 'item',
                                                                    content: [
                                                                        {
                                                                            block: 'employees',
                                                                            elem :'cell',
                                                                            content: {
                                                                                block: 'employees',
                                                                                elem: 'item',
                                                                                content:[
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'image',
                                                                                        content: {
                                                                                            block: 'image',
                                                                                            url: 'https://source.unsplash.com/172x205/?-shirt'
                                                                                        }
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'name',
                                                                                        content: 'Мария иванова'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'post',
                                                                                        content: 'Ногти'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'button',
                                                                                        content: {
                                                                                            block :'button',
                                                                                            mods:{
                                                                                                theme: 'green',
                                                                                                type: 'link'
                                                                                            },
                                                                                           url: '#',
                                                                                            content: 'Записаться'
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        },
                                                                        {
                                                                            block: 'employees',
                                                                            elem :'cell',
                                                                            content: {
                                                                                block: 'employees',
                                                                                elem: 'item',
                                                                                content:[
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'image',
                                                                                        content: {
                                                                                            block: 'image',
                                                                                            url: 'https://source.unsplash.com/172x205/?-car'
                                                                                        }
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'name',
                                                                                        content: 'Мария иванова'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'post',
                                                                                        content: 'Ногти'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'button',
                                                                                        content: {
                                                                                            block :'button',
                                                                                            mods:{
                                                                                                theme: 'green',
                                                                                                type: 'link'
                                                                                            },
                                                                                            url: '#',
                                                                                            content: 'Записаться'
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        },
                                                                        {
                                                                            block: 'employees',
                                                                            elem :'cell',
                                                                            content: {
                                                                                block: 'employees',
                                                                                elem: 'item',
                                                                                content:[
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'image',
                                                                                        content: {
                                                                                            block: 'image',
                                                                                            url: 'https://source.unsplash.com/172x205/?-bla'
                                                                                        }
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'name',
                                                                                        content: 'Мария иванова'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'post',
                                                                                        content: 'Ногти'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'button',
                                                                                        content: {
                                                                                            block :'button',
                                                                                            mods:{
                                                                                                theme: 'green',
                                                                                                type: 'link'
                                                                                            },
                                                                                            url: '#',
                                                                                            content: 'Записаться'
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        },
                                                                        {
                                                                            block: 'employees',
                                                                            elem :'cell',
                                                                            content: {
                                                                                block: 'employees',
                                                                                elem: 'item',
                                                                                content:[
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'image',
                                                                                        content: {
                                                                                            block: 'image',
                                                                                            url: 'https://source.unsplash.com/172x205/?-red'
                                                                                        }
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'name',
                                                                                        content: 'Мария иванова'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'post',
                                                                                        content: 'Ногти'
                                                                                    },
                                                                                    {
                                                                                        block: 'employees',
                                                                                        elem: 'button',
                                                                                        content: {
                                                                                            block :'button',
                                                                                            mods:{
                                                                                                theme: 'green',
                                                                                                type: 'link'
                                                                                            },
                                                                                            url: '#',
                                                                                            content: 'Записаться'
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        },
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'item',
                                                                    content: 'red'
                                                                },
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'col',
                                                    elemMods:{
                                                        mw: 7
                                                    },
                                                    content: [
                                                        {
                                                            block: 'employees-item',
                                                            content: [
                                                                {
                                                                    elem: 'image',
                                                                    content: {
                                                                        block: 'image',
                                                                        url: 'https://source.unsplash.com/228x267/?-shirt'
                                                                    }
                                                                },
                                                                {
                                                                    elem: 'main',
                                                                    content:[
                                                                        {
                                                                            elem: 'name',
                                                                            content: 'Мария иванова'
                                                                        },
                                                                        {
                                                                            elem: 'post',
                                                                            content: 'Ногти'
                                                                        },
                                                                        {
                                                                            elem: 'h2',
                                                                            content: 'О мастере:'
                                                                        },
                                                                        {
                                                                            elem: 'text',
                                                                            content: 'Nail-мастер, опыт работы более 10 лет. Прошла обучение в школе ногтевого дизайна Екатерины Мирошниченко «E.Mi School, OPI.. Проходит постоянное повышение квалификации. Качественное оказание услуг маникюра, педикюра (в т.ч. аппаратного), уходов. Консультация клиентов по домашнему уходу. Работа с Shellac, Гель-лаками различных фирм(CND, OPI, Kodi, Irisk, Bluesky, Tango и т.д.). Выполнение E.Mi дизайнов, а также дизайнов любой сложности: «свитер», роспись акриловыми красками, литьё, жидкие камни, «конфетные ногти», инкрустация стразами, работа со слайдер-дизайнами и т.д.'
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            block: 'achievements',
                                                            mix:[
                                                                {
                                                                    block: 'popup-gallery',
                                                                    js: true
                                                                }
                                                            ],
                                                            content: [
                                                                {
                                                                    block: 'h3',
                                                                    content: 'Достижения'
                                                                },
                                                                {
                                                                    elem: 'row',
                                                                    content: [
                                                                        {
                                                                            elem: 'cell',
                                                                            tag: 'a',
                                                                            attrs:{
                                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                                            },
                                                                            content: {
                                                                                block: 'image',
                                                                                url: 'https://source.unsplash.com/109x141/?-shirt'
                                                                            }
                                                                        },
                                                                        {
                                                                            elem: 'cell',
                                                                            tag: 'a',
                                                                            attrs:{
                                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                                            },
                                                                            content: {
                                                                                block: 'image',
                                                                                url: 'https://source.unsplash.com/109x141/?-shirt'
                                                                            }
                                                                        },
                                                                        {
                                                                            elem: 'cell',
                                                                            tag: 'a',
                                                                            attrs:{
                                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                                            },
                                                                            content: {
                                                                                block: 'image',
                                                                                url: 'https://source.unsplash.com/109x141/?-shirt'
                                                                            }
                                                                        },
                                                                        {
                                                                            elem: 'cell',
                                                                            tag: 'a',
                                                                            attrs:{
                                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                                            },
                                                                            content: {
                                                                                block: 'image',
                                                                                url: 'https://source.unsplash.com/109x141/?-shirt'
                                                                            }
                                                                        },
                                                                        {
                                                                            elem: 'cell',
                                                                            tag: 'a',
                                                                            attrs:{
                                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                                            },
                                                                            content: {
                                                                                block: 'image',
                                                                                url: 'https://source.unsplash.com/109x141/?-shirt'
                                                                            }
                                                                        },
                                                                        {
                                                                            elem: 'cell',
                                                                            tag: 'a',
                                                                            attrs:{
                                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                                            },
                                                                            content: {
                                                                                block: 'image',
                                                                                url: 'https://source.unsplash.com/109x141/?-shirt'
                                                                            }
                                                                        }
                                                                    ]
                                                                }

                                                            ]
                                                        },
                                                        {
                                                            block: 'achievements-text',
                                                            content: 'Мы очень серьёзно подходит к профессионализму мастеров в нашей студии. Именно поэтому, выбрав любого из наших сотрудников, Вы можете быть уверены, что результат превзойдет Ваши ожидания!'
                                                        },
                                                        {
                                                            block: 'achievements-button',
                                                            content:{
                                                                block: 'button',
                                                                mods:{
                                                                    type: 'link',
                                                                    theme: 'green'
                                                                },
                                                                url: "#",
                                                                content: {
                                                                    tag: 'span',
                                                                    content: 'ЗАПИСАТЬСЯ К МАСТЕРУ'
                                                                }
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-20',
                                    },

                                    content: [
                                        {
                                            block: 'employees-item',
                                            content: 'Ресницы'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-30',
                                    },
                                    content: [
                                        {
                                            block: 'employees-item',
                                            content: 'Эпиляция & Шугаринг'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-40',
                                    },
                                    content: [
                                        {
                                            block: 'employees-item',
                                            content: 'Брови'
                                        }

                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-50',
                                    },
                                    content: [
                                        {
                                            block: 'employees-item',
                                            content: 'Стилист'
                                        }

                                    ]
                                }
                            ]
                        }


                    ]
                },
            ]
        },
        {
            block: 'footer'
        }
    ]
};
