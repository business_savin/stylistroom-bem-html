module.exports = {
    block: 'page',
    title: 'Page contacts',
    favicon: '/favicon.ico',
    head: [{
            elem: 'meta',
            attrs: {
                name: 'description',
                content: ''
            }
        },
        {
            elem: 'meta',
            attrs: {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            }
        },
        {
            elem: 'css',
            url: 'https://daneden.github.io/animate.css/animate.min.css'
        },
        {
            elem: 'css',
            url: 'contacts.min.css'
        }
    ],
    scripts: [{
            elem: 'js',
            url: 'https://code.jquery.com/jquery-1.11.1.min.js'
        },
        {
            elem: 'js',
            url: 'https://wowjs.uk/dist/wow.min.js'
        },
        {
            elem: 'js',
            url: '//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU'
        },
        {
            elem: 'js',
            url: 'contacts.min.js'
        }

    ],
    mods: {
        theme: 'islands'
    },
    content: [{
            block: 'header',
            js: true,
        },
        {
            block: 'content',
            content: [{
                    block: 'headline',
                    content: 'Контакты'
                },
                {
                    block: 'wrap',
                    content: [{
                            block: 'contacts',
                            js: true,
                            mix: {
                                block: 'block',
                                mods: {
                                    indent: 'top'
                                }
                            },
                            content: [{
                                elem: 'wrap',
                                content: [

                                    {
                                        elem: 'content',
                                        content: [{
                                                elem: 'cell',
                                                content: [{
                                                        block: 'h2',
                                                        content: 'Адрес:'
                                                    },
                                                    {
                                                        block: 'text',
                                                        content: [{
                                                                block: 'image',
                                                                url: 'https://ic.pics.livejournal.com/ckonovalov/13186847/108869/108869_original.png'
                                                            },
                                                            ' м  Марксистская,  ',
                                                            {
                                                                block: 'image',
                                                                url: 'https://ic.pics.livejournal.com/ckonovalov/13186847/108869/108869_original.png'
                                                            },
                                                            'Таганская',
                                                            {
                                                                tag: 'br'
                                                            },
                                                            'ул.Марксистская, д.3, стр.3,',
                                                            {
                                                                tag: 'br'
                                                            },
                                                            'бизнес-центр "Таганский",',
                                                            {
                                                                tag: 'br'
                                                            },
                                                            '2 подъезд, 2 этаж'

                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                elem: 'cell',
                                                content: [{
                                                        block: 'h2',
                                                        content: 'Время работы:'
                                                    },
                                                    {
                                                        block: 'text',
                                                        content: [
                                                            'Ежедневно: с 10:00 до 23:00',
                                                            {
                                                                tag: 'br'
                                                            },
                                                            'Студия работает до последего клиента'
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                elem: 'cell',
                                                content: [{
                                                        block: 'h2',
                                                        content: 'Телефоны:'
                                                    },
                                                    {
                                                        block: 'text',
                                                        content: [
                                                            '+7 (926) 688-89-84     ',
                                                            {
                                                                block: 'image',
                                                                url: 'https://taxitur.com.ua/wp-content/uploads/2016/12/whatsapp-official-logo-png-download.png'
                                                            },
                                                            {
                                                                block: 'image',
                                                                url: 'https://world-meb.ru/images/viber-Logo.png'
                                                            },
                                                            {
                                                                tag: 'br'
                                                            },
                                                            '+7 (926) 396-03-66     ',
                                                            {
                                                                block: 'image',
                                                                url: 'https://taxitur.com.ua/wp-content/uploads/2016/12/whatsapp-official-logo-png-download.png'
                                                            },
                                                            {
                                                                block: 'image',
                                                                url: 'https://world-meb.ru/images/viber-Logo.png'
                                                            },
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                elem: 'cell',
                                                content: [{
                                                        block: 'h2',
                                                        content: 'Социальные сети:'
                                                    },
                                                    {
                                                        block: 'social',
                                                        content: [{
                                                                image: 'https://isoflex.kinef.ru/upload/instagram_PNG10.png',
                                                                url: 'https://www.instagram.com/stylist.room/'
                                                            },
                                                            {
                                                                image: 'https://online-lostark.ru/wp-content/uploads/2017/10/vk_logo.png',
                                                                url: 'https://vk.com/stylistroom'
                                                            },
                                                            {
                                                                image: 'https://jenniescandles.net/wp-content/uploads/2016/02/Facebook-logo-icon-vectorcopy-big_copy-1020x1024.png',
                                                                url: 'https://www.facebook.com/'
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        block: 'map',
                                        js: true,
                                        attrs: {
                                            id: 'map'
                                        }
                                    }
                                ]
                            }]
                        },
                        {
                            block: 'place',
                            content: [{
                                    block: 'h1',
                                    content: 'КАК дойти?'
                                },
                                {
                                    elem: 'row',
                                    content: [{
                                            elem: 'cell',
                                            mix: [{
                                                    block: 'wow'
                                                },
                                                {
                                                    block: 'bounceInLeft  '
                                                }
                                            ],
                                            attrs: {
                                                'data-wow-delay': '1s',
                                            },
                                            content: [{
                                                    block: 'image',
                                                    url: 'https://source.unsplash.com/234x176/?-black'
                                                },
                                                {
                                                    block: 'text',
                                                    content: 'Вы идете сюда'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'cell',
                                            mix: [{
                                                    block: 'wow'
                                                },
                                                {
                                                    block: 'bounceInLeft  '
                                                }
                                            ],
                                            attrs: {
                                                'data-wow-offset': '0',
                                                'data-wow-delay': '0.9s',
                                                'data-wow-duration': '1s'
                                            },
                                            content: [{
                                                    block: 'image',
                                                    url: 'https://source.unsplash.com/234x176/?-red'
                                                },
                                                {
                                                    block: 'text',
                                                    content: 'Вы идете сюда'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'cell',
                                            mix: [{
                                                    block: 'wow'
                                                },
                                                {
                                                    block: 'bounceInLeft  '
                                                }
                                            ],
                                            attrs: {
                                                'data-wow-offset': '0',
                                                'data-wow-delay': '0.7s',
                                                'data-wow-duration': '1s'
                                            },
                                            content: [{
                                                    block: 'image',
                                                    url: 'https://source.unsplash.com/234x176/?-ori'
                                                },
                                                {
                                                    block: 'text',
                                                    content: 'Вы идете сюда'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'cell',
                                            mix: [{
                                                    block: 'wow'
                                                },
                                                {
                                                    block: 'bounceInLeft  '
                                                }
                                            ],
                                            attrs: {
                                                'data-wow-offset': '0',
                                                'data-wow-delay': '0.5s',
                                                'data-wow-duration': '1s'
                                            },
                                            content: [{
                                                    block: 'image',
                                                    url: 'https://source.unsplash.com/234x176/?-green'
                                                },
                                                {
                                                    block: 'text',
                                                    content: 'Вы идете сюда'
                                                }
                                            ]
                                        }
                                    ]
                                }

                            ]
                        }
                    ]
                }

            ]
        },
        {
            block: 'footer'

        }
    ]
};
