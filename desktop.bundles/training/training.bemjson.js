module.exports = {
    block: 'page',
    title: 'Page training',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'training.min.css' }
    ],
    scripts: [
        { elem: 'js', url: 'https://code.jquery.com/jquery-1.11.1.min.js' },
        { elem: 'js', url: 'training.min.js' }
    ],
    mods: { theme: 'islands' },
    content: [
        {
            block: 'header',
            js: true,
            content: [
                'header content goes here'
            ]
        },
        
        {
            block: 'content',
            content:[
                {
                    block: 'headline',
                    content: 'ОБУЧЕНИЕ'
                },
                {
                    block: 'tabs',
                    mods: {
                        theme: 'green',
                        type: 'horizontally'
                    },
                    js: true,
                    content: [
                        {
                            elem: 'navigation',
                            content: [
                                {
                                    elem: 'table',
                                    content: [
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-10'
                                            },
                                            mix: {
                                                block: 'tabs',
                                                elem: 'tab--active'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Ногтевой сервис'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-20'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Ресницы'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-30'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Эпиляция & Шугаринг'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-40'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Брови'
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'tab',
                                            attrs: {
                                                href: '#tab-50'
                                            },
                                            content: [
                                                {
                                                    tag: 'span',
                                                    content: 'Cтилист'
                                                }
                                            ]
                                        },
                                    ]
                                }
                                
                            ]
                        },
                        {
                            block: 'tabs',
                            elem: 'wrap',
                            content: [
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-10',
                                    },
                                    mix:[
                                        {
                                            block: 'active'
                                        }
                                    ],
                                    content: [
                                        {
                                            block: 'training',
                                            mods:{
                                                indent: 'top'
                                            },
                                            content: [
                                                {
                                                    elem: 'item',
                                                    content: [
                                                        {
                                                            elem: 'image',
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/148x148/?-shirt'
                                                            }
                                                        },
                                                        {
                                                            elem: 'middle',
                                                            content: [
                                                                {
                                                                    elem: 'h1',
                                                                    content: 'курс по наращиванию ногтей (SHELLAC)'
                                                                },
                                                                {
                                                                    elem: 'text',
                                                                    content: 'Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач.'
                                                                },
                                                                {
                                                                    elem: 'tools',
                                                                    content: [
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                'Цена: ',
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: '1990'
                                                                                },
                                                                                'Р'
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                'Длительность:',
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: '8'
                                                                                },
                                                                                'часов'
                                                                            ]
                                                                        }, {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                {
                                                                                    block: 'link',
                                                                                    url: '#',
                                                                                    content: 'Подробнее о курсе'
                                                                                },
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'button',
                                                            content: [
                                                                {
                                                                    block: 'button',
                                                                    mods: {
                                                                        type: 'link',
                                                                        theme: 'green'
                                                                    },
                                                                    url: '#',
                                                                    content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'item',
                                                    content: [
                                                        {
                                                            elem: 'image',
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/148x148/?-shirt'
                                                            }
                                                        },
                                                        {
                                                            elem: 'middle',
                                                            content: [
                                                                {
                                                                    elem: 'h1',
                                                                    content: 'курс по наращиванию ногтей (SHELLAC)'
                                                                },
                                                                {
                                                                    elem: 'text',
                                                                    content: 'Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач.'
                                                                },
                                                                {
                                                                    elem: 'tools',
                                                                    content: [
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                'Цена: ',
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: '1990'
                                                                                },
                                                                                'Р'
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                'Длительность:',
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: '8'
                                                                                },
                                                                                'часов'
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                {
                                                                                    block: 'link',
                                                                                    url: '#',
                                                                                    content: 'Подробнее о курсе'
                                                                                },
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'button',
                                                            content: [
                                                                {
                                                                    block: 'button',
                                                                    mods: {
                                                                        type: 'link',
                                                                        theme: 'green'
                                                                    },
                                                                    url: '#',
                                                                    content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'button',
                                                    elemMods: {
                                                        align: 'right'
                                                    },
                                                    content: [
                                                        {
                                                            block: 'button',
                                                            mods: {
                                                                type: 'link',
                                                                theme: 'gray'
                                                            },
                                                            url: '#',
                                                            content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    block: 'tabs',
                                    elem: 'content',
                                    attrs: {
                                        id: 'tab-20',
                                    },
                                 
                                    content: [
                                        {
                                            block: 'training',
                                            mods:{
                                                indent: 'top'
                                            },
                                            content: [
                                                {
                                                    elem: 'item',
                                                    content: [
                                                        {
                                                            elem: 'image',
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/148x148/?-rei'
                                                            }
                                                        },
                                                        {
                                                            elem: 'middle',
                                                            content: [
                                                                {
                                                                    elem: 'h1',
                                                                    content: 'курс по наращиванию ресниц (SHELLAC)'
                                                                },
                                                                {
                                                                    elem: 'text',
                                                                    content: 'Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач.'
                                                                },
                                                                {
                                                                    elem: 'tools',
                                                                    content: [
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                'Цена: ',
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: '1990'
                                                                                },
                                                                                'Р'
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                'Длительность:',
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: '8'
                                                                                },
                                                                                'часов'
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tools-cell',
                                                                            content: [
                                                                                {
                                                                                    block: 'link',
                                                                                    url: '#',
                                                                                    content: 'Подробнее о курсе'
                                                                                },
                                                                            ]
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            elem: 'button',
                                                            content: [
                                                                {
                                                                    block: 'button',
                                                                    mods: {
                                                                        type: 'link',
                                                                        theme: 'green'
                                                                    },
                                                                    url: '#',
                                                                    content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'button',
                                                    elemMods: {
                                                        align: 'right'
                                                    },
                                                    content: [
                                                        {
                                                            block: 'button',
                                                            mods: {
                                                                type: 'link',
                                                                theme: 'gray'
                                                            },
                                                            url: '#',
                                                            content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                        
                                    ]
                                },
                            ]
                        }
                        
        
                    ]
                },
            ]
        },
        {
            block: 'footer'
        }
    ]
};
