module.exports = {
    block: 'page',
    title: 'Title of the page',
    favicon: '/favicon.ico',
    head: [
        { elem: 'meta', attrs: { name: 'description', content: '' } },
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: '/static/Magnific-Popup-master/dist/magnific-popup.css' },
        { elem: 'css', url: 'index.min.css' }
    ],
    scripts: [
        { elem: 'js', url: 'https://code.jquery.com/jquery-1.11.1.min.js' },
        {elem: 'js', url: '//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU'},
        { elem: 'js', url: '/static/parallax.js-1.5.0/parallax.min.js' },
        { elem: 'js', url: '/static/Magnific-Popup-master/dist/jquery.magnific-popup.min.js' },

        { elem: 'js', url: 'index.min.js' }
    ],
    mods: { theme: 'islands' },
    content: [
        {
            block: 'header',
            js: true,
            content: [
                'header content goes here'
            ]
        },
        {
            block: 'content',
            content: [

                {
                    block: 'promo',
                    mix: {
                        block: 'parallax-window'
                    },
                    attrs: {
                        'data-parallax': 'scroll',
                        'data-image-src': '/desktop.blocks/promo/image.png'
                    },
                    content: [
                        {
                            elem: 'content',
                            content: [
                                {
                                    elem: 'cell',
                                    content: [
                                        {
                                            block: 'h1',
                                            content: 'Студия красоты, которую вы искали!'
                                        },
                                        {
                                            block: 'text',
                                            content: '«Stylist Room» — это уютная студия красоты, расположенная в историческом центре в 2-3 минутах от ст.м.Марксистская, ст.м.Таганская (кольцевая).'
                                        },
                                        {
                                            block: 'button',
                                            mods: {
                                                theme: 'green',
                                                type: 'link'
                                            },
                                            url: 'https://bem.info/',
                                            content: 'ЗАПИСАТЬСЯ'
                                        }
                                    ]
                                },
                                {
                                    elem: 'cell',
                                    elemMods: {
                                        align: 'right'
                                    },
                                    content: [
                                        {
                                            elem: 'app-info',
                                            content: [
                                                {
                                                    block: 'text',
                                                    content: 'Скачайте наше приложение «Stylist Room»:'
                                                },
                                                {
                                                    elem: 'app--image',
                                                    content: [
                                                        {
                                                            block: 'link',
                                                            url: '#',
                                                            content: [
                                                                {
                                                                    block: 'image',
                                                                    url: '/static/appStore.png'
                                                                },
                                                            ]
                                                        },
                                                        {
                                                            block: 'link',
                                                            url: '#',
                                                            content: [
                                                                {
                                                                    block: 'image',
                                                                    url: '/static/googlePay.png'
                                                                }
                                                            ]
                                                        },

                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },

                            ]
                        }
                    ]
                },

                {
                    block: 'wrap',
                    content:[
                        {
                            block: 'row',
                            content: [
                                {
                                    elem: 'col',
                                    elemMods:{
                                        mw: 9
                                    },
                                    content: [
                                        {
                                            block: 'h1',
                                            content: 'Услуги и цены'
                                        },
                                        {
                                            block: 'block',
                                            content: [
                                                {
                                                    block: 'tabs',
                                                    mods: {
                                                        theme: 'green',
                                                        type: 'row'
                                                    },
                                                    js: true,
                                                    content: [
                                                        {
                                                            elem: 'navigation',
                                                            content: [
                                                                {
                                                                    elem: 'table',
                                                                    content: [
                                                                        {
                                                                            elem: 'tab',
                                                                            attrs: {
                                                                                href: '#tab-10'
                                                                            },
                                                                            mix: {
                                                                                block: 'tabs',
                                                                                elem: 'tab--active'
                                                                            },
                                                                            content: [
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: 'Услуги стилиста'
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tab',
                                                                            attrs: {
                                                                                href: '#tab-20'
                                                                            },
                                                                            content: [
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: 'Ногтевой сервис'
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tab',
                                                                            attrs: {
                                                                                href: '#tab-30'
                                                                            },
                                                                            content: [
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: 'Брови'
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tab',
                                                                            attrs: {
                                                                                href: '#tab-40'
                                                                            },
                                                                            content: [
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: 'Макияж'
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tab',
                                                                            attrs: {
                                                                                href: '#tab-50'
                                                                            },
                                                                            content: [
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: 'Ресницы'
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            elem: 'tab',
                                                                            attrs: {
                                                                                href: '#tab-60'
                                                                            },
                                                                            content: [
                                                                                {
                                                                                    tag: 'span',
                                                                                    content: 'Эпиляция & Шугаринг'
                                                                                }
                                                                            ]
                                                                        }
                                                                    ]
                                                                }

                                                            ]
                                                        },
                                                        {
                                                            block: 'tabs',
                                                            elem: 'wrap',
                                                            content: [
                                                                {
                                                                    block: 'tabs',
                                                                    elem: 'content',
                                                                    attrs: {
                                                                        id: 'tab-10',
                                                                    },
                                                                    mix:[
                                                                        {
                                                                            block: 'active'
                                                                        }
                                                                    ],
                                                                    content: [
                                                                        {
                                                                            block: 'h2',
                                                                            content:' Цены'
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка детская от 5 лет'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  700  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка детская до 10 лет'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  1000  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка детская до 15 лет'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  1500  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {tag: 'br'},
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка мужская'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  1500  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка под машинку'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  500  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка усов/бороды'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  600  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {tag: 'br'},
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка женская в технике «TONY & GUY»'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  2000  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка женская в технике «Vidal Sassoon»'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  2300  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },

                                                                    ]
                                                                },
                                                                {
                                                                    block: 'tabs',
                                                                    elem: 'content',
                                                                    attrs: {
                                                                        id: 'tab-20',
                                                                    },

                                                                    content: [
                                                                        {
                                                                            block: 'h2',
                                                                            content:' Цены2'
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка детская от 5 лет'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  700  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка детская до 10 лет'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  1000  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка детская до 15 лет'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  1500  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {tag: 'br'},
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка мужская'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  1500  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка под машинку'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  500  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка усов/бороды'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  600  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {tag: 'br'},
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка женская в технике «TONY & GUY»'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  2000  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'row',
                                                                            content: [
                                                                                {
                                                                                    block: 'text',
                                                                                    content: 'Стрижка женская в технике «Vidal Sassoon»'
                                                                                },
                                                                                {
                                                                                    block: 'text',
                                                                                    content: [
                                                                                        '— от',
                                                                                        {
                                                                                            tag: 'b',
                                                                                            content: '  2300  '
                                                                                        },
                                                                                        'рублей'
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        },

                                                                    ]
                                                                },
                                                            ]
                                                        }


                                                    ]
                                                },

                                            ]
                                        },
                                        {
                                          block: 'form',
                                          js: true
                                        },
                                        {
                                            block: 'block',
                                            mods:{
                                                indent: 'top'
                                            },
                                            content: [
                                                {
                                                    block: 'employees',
                                                    content: [

                                                        {
                                                            elem: 'wrap',
                                                            content: [
                                                                {
                                                                    block: 'h2',
                                                                    content: 'Сотрудники'
                                                                },
                                                                {
                                                                    elem :'cell',
                                                                    content: {
                                                                        elem: 'item',
                                                                        content:[
                                                                            {
                                                                                elem: 'image',
                                                                                content: {
                                                                                    block: 'image',
                                                                                    url: 'https://source.unsplash.com/172x205/?-shirt'
                                                                                }
                                                                            },
                                                                            {
                                                                                elem: 'name',
                                                                                content: 'Мария иванова'
                                                                            },
                                                                            {
                                                                                elem: 'post',
                                                                                content: 'Ногти'
                                                                            },
                                                                            {
                                                                                elem: 'button',
                                                                                content: {
                                                                                    block :'button',
                                                                                    mods:{
                                                                                        theme: 'green',
                                                                                        type: 'link'
                                                                                    },
                                                                                    content: 'Записаться'
                                                                                }
                                                                            }
                                                                        ]
                                                                    }
                                                                },
                                                                {
                                                                    elem :'cell',
                                                                    content: {
                                                                        elem: 'item',
                                                                        content:[
                                                                            {
                                                                                elem: 'image',
                                                                                content: {
                                                                                    block: 'image',
                                                                                    url: 'https://source.unsplash.com/172x205/?-pup'
                                                                                }
                                                                            },
                                                                            {
                                                                                elem: 'name',
                                                                                content: 'Мария иванова'
                                                                            },
                                                                            {
                                                                                elem: 'post',
                                                                                content: 'Ногти'
                                                                            },
                                                                            {
                                                                                elem: 'button',
                                                                                content: {
                                                                                    block :'button',
                                                                                    mods:{
                                                                                        theme: 'green',
                                                                                        type: 'link'
                                                                                    },
                                                                                    content: 'Записаться'
                                                                                }
                                                                            }
                                                                        ]
                                                                    }
                                                                },
                                                                {
                                                                    elem :'cell',
                                                                    content: {
                                                                        elem: 'item',
                                                                        content:[
                                                                            {
                                                                                elem: 'image',
                                                                                content: {
                                                                                    block: 'image',
                                                                                    url: 'https://source.unsplash.com/172x205/?-love'
                                                                                }
                                                                            },
                                                                            {
                                                                                elem: 'name',
                                                                                content: 'Мария иванова'
                                                                            },
                                                                            {
                                                                                elem: 'post',
                                                                                content: 'Ногти'
                                                                            },
                                                                            {
                                                                                elem: 'button',
                                                                                content: {
                                                                                    block :'button',
                                                                                    mods:{
                                                                                        theme: 'green',
                                                                                        type: 'link'
                                                                                    },
                                                                                    content: 'Записаться'
                                                                                }
                                                                            }
                                                                        ]
                                                                    }
                                                                },
                                                                {
                                                                    elem :'cell',
                                                                    content: {
                                                                        elem: 'item',
                                                                        content:[
                                                                            {
                                                                                elem: 'image',
                                                                                content: {
                                                                                    block: 'image',
                                                                                    url: 'https://source.unsplash.com/172x205/?-fest'
                                                                                }
                                                                            },
                                                                            {
                                                                                elem: 'name',
                                                                                content: 'Мария иванова'
                                                                            },
                                                                            {
                                                                                elem: 'post',
                                                                                content: 'Ногти'
                                                                            },
                                                                            {
                                                                                elem: 'button',
                                                                                content: {
                                                                                    block :'button',
                                                                                    mods:{
                                                                                        theme: 'green',
                                                                                        type: 'link'
                                                                                    },
                                                                                    content: 'Записаться'
                                                                                }
                                                                            }
                                                                        ]
                                                                    }
                                                                },
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    elem: 'col',
                                    elemMods:{
                                        mw: 3
                                    },
                                    content: [
                                        {
                                            block: 'h1',
                                            content: 'Акции'
                                        },
                                        {
                                            block: 'shares',
                                            content:[
                                                {
                                                    block: 'image',
                                                    url: '/static/shares-1.png'
                                                },
                                                {
                                                    block: 'button',
                                                    mods:{
                                                        type: 'link'
                                                    },
                                                    url: '#',
                                                    content: 'ЗАПИСАТЬСЯ'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'shares',
                                            content:[
                                                {
                                                    block: 'image',
                                                    url: '/static/shares-2.png'
                                                },
                                                {
                                                    block: 'button',
                                                    mods:{
                                                        type: 'link'
                                                    },
                                                    url: '#',
                                                    content: 'ЗАПИСАТЬСЯ'
                                                }
                                            ]


                                        },
                                        {
                                            block: 'shares',
                                            content:[
                                                {
                                                    block: 'image',
                                                    url: '/static/shares-3.png'
                                                },

                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'training',
                            mix: {
                                block: 'block',

                                mods: {
                                    indent: 'top'
                                }
                            },
                            content: [
                                {
                                    elem: 'wrap',
                                    content: [
                                        {
                                            block: 'h1',
                                            mods: {

                                            },
                                            content: 'ОБУЧЕНИЕ'
                                        },
                                        {
                                            elem: 'item',
                                            content: [
                                                {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/148x148/?-shirt'
                                                    }
                                                },
                                                {
                                                    elem: 'middle',
                                                    content: [
                                                        {
                                                            elem: 'h1',
                                                            content: 'курс по наращиванию ногтей (SHELLAC)'
                                                        },
                                                        {
                                                            elem: 'text',
                                                            content: 'Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач.'
                                                        },
                                                        {
                                                            elem: 'tools',
                                                            content: [
                                                                {
                                                                    elem: 'tools-cell',
                                                                    content: [
                                                                        'Цена: ',
                                                                        {
                                                                            tag: 'span',
                                                                            content: '1990'
                                                                        },
                                                                        'Р'
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'tools-cell',
                                                                    content: [
                                                                        'Длительность:',
                                                                        {
                                                                            tag: 'span',
                                                                            content: '8'
                                                                        },
                                                                        'часов'
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'button',
                                                    content: [
                                                        {
                                                            block: 'button',
                                                            mods: {
                                                                type: 'link',
                                                                theme: 'green'
                                                            },
                                                            url: '#',
                                                            content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'item',
                                            content: [
                                                {
                                                    elem: 'image',
                                                    content: {
                                                        block: 'image',
                                                        url: 'https://source.unsplash.com/148x148/?-shirt'
                                                    }
                                                },
                                                {
                                                    elem: 'middle',
                                                    content: [
                                                        {
                                                            elem: 'h1',
                                                            content: 'курс по наращиванию ногтей (SHELLAC)'
                                                        },
                                                        {
                                                            elem: 'text',
                                                            content: 'Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач.'
                                                        },
                                                        {
                                                            elem: 'tools',
                                                            content: [
                                                                {
                                                                    elem: 'tools-cell',
                                                                    content: [
                                                                        'Цена: ',
                                                                        {
                                                                            tag: 'span',
                                                                            content: '1990'
                                                                        },
                                                                        'Р'
                                                                    ]
                                                                },
                                                                {
                                                                    elem: 'tools-cell',
                                                                    content: [
                                                                        'Длительность:',
                                                                        {
                                                                            tag: 'span',
                                                                            content: '8'
                                                                        },
                                                                        'часов'
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'button',
                                                    content: [
                                                        {
                                                            block: 'button',
                                                            mods: {
                                                                type: 'link',
                                                                theme: 'green'
                                                            },
                                                            url: '#',
                                                            content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            elem: 'button',
                                            elemMods: {
                                                align: 'right'
                                            },
                                            content: [
                                                {
                                                    block: 'button',
                                                    mods: {
                                                        type: 'link',
                                                        theme: 'gray'
                                                    },
                                                    url: '#',
                                                    content: 'ЗАПИСАТЬСЯ НА КУРС'
                                                }
                                            ]
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            block: 'reviews',
                            mix: {
                                block: 'block',

                                mods: {
                                    indent: 'top'
                                }
                            },
                            content: [
                                {
                                    elem: 'wrap',
                                    content: [
                                        {
                                            block: 'h2',
                                            content: 'Отзывы наших клиентов'
                                        },
                                        {
                                            elem: 'content',
                                            content: [
                                                {
                                                    elem: 'cell',
                                                    content: {
                                                        elem: 'image',
                                                        content: {
                                                            block: 'image',
                                                            url: 'https://source.unsplash.com/283x283/?-row'
                                                        }
                                                    }
                                                },
                                                {
                                                    elem: 'cell',
                                                    content: {
                                                        elem: 'image',
                                                        content: {
                                                            block: 'image',
                                                            url: 'https://source.unsplash.com/283x283/?-coll'
                                                        }
                                                    }
                                                },
                                                {
                                                    elem: 'cell',
                                                    content: {
                                                        elem: 'image',
                                                        content: {
                                                            block: 'image',
                                                            url: 'https://source.unsplash.com/283x283/?-blue'
                                                        }
                                                    }
                                                },
                                                {
                                                    elem: 'cell',
                                                    content: {
                                                        elem: 'image',
                                                        content: {
                                                            block: 'image',
                                                            url: 'https://source.unsplash.com/283x283/?-dick'
                                                        }
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            block: 'button',
                                            mods:{
                                                type: 'link',
                                                theme: 'green'
                                            },
                                            url: '#',
                                            content: 'Перейти в отзывы'
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            block: 'examples',
                            mix: {
                                block: 'block',
                                mods: {
                                    indent: 'top'
                                }
                            },
                            content: [
                                {
                                    elem: 'wrap',
                                    content: [
                                        {
                                            block: 'h1',
                                            content: 'Посмотрите наши работы'
                                        },
                                        {
                                            block: 'tabs',
                                            mods: {
                                                theme: 'light'
                                            },
                                            js: true,
                                            content: [
                                                {
                                                    elem: 'navigation',
                                                    content: [
                                                        {
                                                            elem: 'tab',
                                                            attrs: {
                                                                href: '#tab-1'
                                                            },
                                                            mix: {
                                                                block: 'tabs',
                                                                elem: 'tab--active'
                                                            },
                                                            content: 'Макияж'
                                                        },
                                                        {
                                                            elem: 'tab',
                                                            attrs: {
                                                                href: '#tab-2'
                                                            },
                                                            content: 'Ногтевой сервис'
                                                        },
                                                        {
                                                            elem: 'tab',
                                                            attrs: {
                                                                href: '#tab-3'
                                                            },
                                                            content: 'Ресницы'
                                                        },
                                                        {
                                                            elem: 'tab',
                                                            attrs: {
                                                                href: '#tab-4'
                                                            },
                                                            content: 'Эпиляция & Шугаринг'
                                                        },
                                                        {
                                                            elem: 'tab',
                                                            attrs: {
                                                                href: '#tab-5'
                                                            },
                                                            content: 'Брови'
                                                        },
                                                        {
                                                            elem: 'tab',
                                                            attrs: {
                                                                href: '#tab-6'
                                                            },
                                                            content: 'Услуги стилиста'
                                                        },
                                                    ]
                                                },
                                                {
                                                    block: 'tabs',
                                                    elem: 'content',
                                                    attrs: {
                                                        id: 'tab-1',
                                                    },
                                                    mix:[
                                                        {
                                                            block: 'active'
                                                        },
                                                        {
                                                            block: 'popup-gallery',
                                                            js: true
                                                        }
                                                    ],
                                                    content: [
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-1'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-1'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-2'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-2'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-3'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-3'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-4'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-4'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-5'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-5'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-6'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-6'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-7'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-7'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-8'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-8'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-9'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-9'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-10'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-10'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-11'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-11'
                                                            }
                                                        },
                                                        {
                                                            elem: 'cell',
                                                            tag: 'a',
                                                            attrs:{
                                                                href: 'https://source.unsplash.com/283x283/?-12'
                                                            },
                                                            content: {
                                                                block: 'image',
                                                                url: 'https://source.unsplash.com/283x283/?-12'
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    block: 'tabs',
                                                    elem: 'content',
                                                    attrs: {
                                                        id: 'tab-2',
                                                    },
                                                    content: '2'
                                                },
                                                {
                                                    block: 'tabs',
                                                    elem: 'content',
                                                    attrs: {
                                                        id: 'tab-3',
                                                    },
                                                    content: '3'
                                                },
                                                {
                                                    block: 'tabs',
                                                    elem: 'content',
                                                    attrs: {
                                                        id: 'tab-4',
                                                    },
                                                    content: '4'
                                                },
                                                {
                                                    block: 'tabs',
                                                    elem: 'content',
                                                    attrs: {
                                                        id: 'tab-5',
                                                    },
                                                    content: '5'
                                                },
                                                {
                                                    block: 'tabs',
                                                    elem: 'content',
                                                    attrs: {
                                                        id: 'tab-6',
                                                    },
                                                    content: '6'
                                                }
                                            ]
                                        },
                                        {
                                            block: 'button',
                                            mods:{
                                                type: 'link',
                                                theme: 'green'
                                            },
                                            url: '#',
                                            content: 'Перейти в фотогалерею'
                                        }
                                    ]
                                }

                            ]
                        },
                        {
                            block: 'contacts',
                            mix: {
                                block: 'block',
                                mods: {
                                    indent: 'top'
                                }
                            },
                            content: [
                                {
                                    elem: 'wrap',
                                    content: [
                                        {
                                            block: 'h1',
                                            content: 'Контакты'
                                        },
                                        {
                                            elem: 'content',
                                            content: [
                                                {
                                                    elem: 'cell',
                                                    content: [
                                                        {
                                                            block: 'h2',
                                                            content: 'Адрес:'
                                                        },
                                                        {
                                                            block: 'text',
                                                            content: [
                                                                {
                                                                    block: 'image',
                                                                    url: 'https://ic.pics.livejournal.com/ckonovalov/13186847/108869/108869_original.png'
                                                                },
                                                                ' м  Марксистская,  ',
                                                                {
                                                                    block: 'image',
                                                                    url: 'https://ic.pics.livejournal.com/ckonovalov/13186847/108869/108869_original.png'
                                                                },
                                                                'Таганская',
                                                                {
                                                                    tag: 'br'
                                                                },
                                                                'ул.Марксистская, д.3, стр.3,',
                                                                {
                                                                    tag: 'br'
                                                                },
                                                                'бизнес-центр "Таганский",',
                                                                {
                                                                    tag: 'br'
                                                                },
                                                                '2 подъезд, 2 этаж'

                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'cell',
                                                    content: [
                                                        {
                                                            block: 'h2',
                                                            content: 'Время работы:'
                                                        },
                                                        {
                                                            block: 'text',
                                                            content:[
                                                                'Ежедневно: с 10:00 до 23:00',
                                                                {
                                                                    tag: 'br'
                                                                },
                                                                'Студия работает до последего клиента'
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'cell',
                                                    content: [
                                                        {
                                                            block: 'h2',
                                                            content: 'Телефоны:'
                                                        },
                                                        {
                                                            block: 'text',
                                                            content: [
                                                                '+7 (926) 688-89-84     ',
                                                                {
                                                                    block: 'image',
                                                                    url: 'https://taxitur.com.ua/wp-content/uploads/2016/12/whatsapp-official-logo-png-download.png'
                                                                },
                                                                {
                                                                    block: 'image',
                                                                    url: 'https://world-meb.ru/images/viber-Logo.png'
                                                                },
                                                                {
                                                                    tag: 'br'
                                                                },
                                                                '+7 (926) 396-03-66     ',
                                                                {
                                                                    block: 'image',
                                                                    url: 'https://taxitur.com.ua/wp-content/uploads/2016/12/whatsapp-official-logo-png-download.png'
                                                                },
                                                                {
                                                                    block: 'image',
                                                                    url: 'https://world-meb.ru/images/viber-Logo.png'
                                                                },
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    elem: 'cell',
                                                    content: [
                                                        {
                                                            block: 'h2',
                                                            content: 'Социальные сети:'
                                                        }
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            block: 'map',
                                            js: true,
                                            attrs:{
                                                id: 'map'
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }

            ]
        },
        {
            block: 'footer'

        }
    ]
};
