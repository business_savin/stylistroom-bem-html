modules.define('slider', ['i-bem-dom'], function(provide, bemDom) {

provide(bemDom.declBlock(this.name, {
    onSetMod: {
        js: {
            inited: function() {
                $('.slider').owlCarousel({
                    loop:true,
                    margin:0,
                    navigation:false,
                    items: 1,
                    stageOuterClass: 'slider__stage-outer',
                    stageClass: 'slider__stage',
                    dragClass:'slider_drag_true',
                    navContainerClass:'slider__nav',
                    dotsClass: 'slider__dots',
                    navClass:["slider__prev","slider__next"],
                    dotClass: 'slider__dot',
                    loadedClass: 'slider_loaded',
                    autoplay:false,
                    autoplayTimeout:2500,
                    autoplayHoverPause:true
                })
            }
        }
    }
}));

});
