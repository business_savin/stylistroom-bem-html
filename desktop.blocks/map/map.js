modules.define('map', ['i-bem-dom'], function(provide, bemDom) {

    provide(bemDom.declBlock(this.name, {
        onSetMod: {
            js: {
                inited: function() {
                    if (document.querySelectorAll('#map').length) {
    
                        var myZoom = 14;
    
                        var center =  [55.738171, 37.638701];
    
    
                        var myEvents = ['searchControl', 'scrollZoom', 'rightMouseButtonMagnifier'];
                        var myControls = ['zoomControl'];
    
                        if (window.innerWidth < 768) {
                            myEvents = ['searchControl', 'drag', 'scrollZoom', 'rightMouseButtonMagnifier'];
                            myControls = [];
                        }
                        ymaps.ready(initMap);
    
                        function initMap() {
                            var myMap = new ymaps.Map("map", {
                                center: center,
                                zoom: myZoom,
                                controls: myControls
                            });
                            myMap.behaviors.disable(myEvents);
                            var myPlacemark = new ymaps.Placemark(
                                [55.738171, 37.638701],
                                {}
                            );
    
                            myMap.geoObjects
                                .add(myPlacemark);
                        }
                    }
                }
            }
        }
    }));
    
    });