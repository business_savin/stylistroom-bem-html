block('logo').content()(function() {
    return {
        block: 'image',
        mix: { block: 'logo', elem: 'image' },
        url: '/desktop.blocks/logo/logo.png'
    }
});
block('logo')({ tag: 'a', attrs: { href: '/' } })