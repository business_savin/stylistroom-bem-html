modules.define('tabs', ['i-bem-dom'], function(provide, bemDom) {

    provide(bemDom.declBlock(this.name, {
        onSetMod: {
            js: {
                inited: function() {
                    $(".tabs__tab").click(function( event ) {
                        event.preventDefault();
                        $(this).addClass("tabs__tab--active");
                        $(this).siblings().removeClass("tabs__tab--active");
                        var tab = $(this).attr("href");
                        $('.tabs__content').removeClass('active');
                        $(tab).addClass('active');
                        console.log(tab);
                        $(tab).show();
                    });

                }
            }
        }
    }));

    });
