block('social').content()(function() {
    return this.ctx.content.map(function (item) {
      return [{
        block: 'link',
        url: item.url,
        mix:{
            block: 'social',
            elem: 'item'
        },
        content:[{
          block:'image',
          url:item.image
        }]
      }]
    })
});
