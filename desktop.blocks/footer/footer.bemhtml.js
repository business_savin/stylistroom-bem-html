block('footer').content()(function () {
    return [
        {
            elem: 'content',
            content: [
                {
                    elem: 'logo',
                    content: {
                        block: 'logo'
                    }
                },
                {
                    elem: 'middle',
                    content: [
                        {
                            elem: 'menu',
                            content: [
                                {
                                    elem: 'cell',
                                    content: [
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Главная'
                                            }
                                        },
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Услуги'
                                            }
                                        },
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Прайс-лист'
                                            }
                                        }
                                    ]
                                },
                                {
                                    elem: 'cell',
                                    content: [
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Обучение'
                                            }
                                        },
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Отзывы'
                                            }
                                        },
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Сотрудники'
                                            }
                                        }
                                    ]
                                },
                                {
                                    elem: 'cell',
                                    content: [
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Фотогалерея'
                                            }
                                        },
                                        {
                                            elem: 'link',
                                            content: {
                                                block: 'link',
                                                url: '#',
                                                content: 'Контакты'
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            elem: 'contacts',
                            content: [
                                {
                                    block: 'text',
                                    content: [
                                        {
                                            block: 'image',
                                            url: 'https://ic.pics.livejournal.com/ckonovalov/13186847/108869/108869_original.png'
                                        },
                                        {
                                            tag: 'span',
                                            content: 'Марксистская'
                                        }
                                    ]
                                },
                                {
                                    block: 'text',
                                    content: [
                                        {
                                            block: 'image',
                                            url: 'https://ic.pics.livejournal.com/ckonovalov/13186847/108869/108869_original.png'
                                        },
                                        {
                                            tag: 'span',
                                            content: 'Таганская'
                                        }
                                    ]
                                },
                                {
                                    block: 'text',
                                    content: 'ул.Марксистская, д.3, стр.3,  бизнес-центр "Таганский", 2 подъезд, 2 этаж'
                                },
                            ]
                        },
                        {
                            elem: 'info',
                            content: [
                                {
                                    block: 'text',
                                    mods: {
                                        size: '16',
                                        weight: 'light',
                                        alight: 'right'
                                    },
                                    content: '+7 (926) 396-03-66'
                                },
                                {
                                    block: 'text',
                                    mods: {
                                        size: '16',
                                        weight: 'light',
                                        alight: 'right'
                                    },
                                    content: '+7 (926) 396-03-66'
                                },
                                {
                                    block: 'button',
                                    tag: 'a',
                                    attrs:{
                                        href: '#'
                                    },
                                    mods: {
                                        theme: 'green'
                                    },
                                    content: 'ЗАПИСАТЬСЯ'
                                }
                            ]
                        }
                    ]
                },
               
            ]
            
        },
        {
            elem: 'two',
            content: [
                {
                    elem: 'content-two',
                    content: [
                        {
                            block: 'text-copy',
                            content: 'Copyright © 2018 Все права защищены'
                        },
                        {
                            block: 'text-author',
                            content: [
                                'Сайт разработан:',
                                {
                                    tag: 'a',
                                    attrs:{
                                        href: '#'
                                    },
                                    content: 'COBBI'
                                }
                                
                            ]
                        }
                    ]
                }
               
            ]
        }
        
    ]
});
