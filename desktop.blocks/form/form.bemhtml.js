block('form').content()(function() {
    return [
      {
        elem: 'wrap',
        tag: 'span',
        mix:[
          {
            block: 'row',
            elem: 'content'
          }
        ]
      }
    ]
});
