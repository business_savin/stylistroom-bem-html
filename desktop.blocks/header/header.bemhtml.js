block('header').content()(function() {
    return[
        {
            elem: 'content',
            content: [
                {
                    elem: 'logo',
                    content:{
                        block: 'logo'
                    }
                },
                {
                    elem: 'middle',
                    content: [
                        {
                            elem: 'menu',
                            content: [
                                {
                                    elem: 'link',
                                    content:{
                                        block: 'link',
                                        url: '/desktop.bundles/index/index.html',
                                        content: 'Главная'
                                    }
                                },
                                {
                                    elem: 'link',
                                    content:{
                                        block: 'link',
                                        url: '/desktop.bundles/services/services.html',
                                        content: 'Услуги и цены'
                                    }
                                },
                                {
                                    elem: 'link',
                                    content:{
                                        block: 'link',
                                        url: '/desktop.bundles/training/training.html',
                                        content: 'Обучение'
                                    }
                                },
                                {
                                    elem: 'link',
                                    content:{
                                        block: 'link',
                                        url: '/desktop.bundles/photo-gallery/photo-gallery.html',
                                        content: 'Фотогалерея'
                                    }
                                },
                                {
                                    elem: 'link',
                                    content:{
                                        block: 'link',
                                        url: '/desktop.bundles/reviews/reviews.html',
                                        content: 'Отзывы'
                                    }
                                },
                                {
                                    elem: 'link',
                                    content:{
                                        block: 'link',
                                        url: '/desktop.bundles/contacts/contacts.html',
                                        content: 'Контакты'
                                    }
                                },
                            ]
                        },
                        {
                            elem: 'info',
                            content: [
                                {
                                    block: 'text',
                                    mods:{
                                        size: '16',
                                        weight: 'light',
                                        alight: 'right'
                                    },
                                    content: '+7 (926) 396-03-66'
                                },
                                {
                                    block: 'text',
                                    mods:{
                                        size: '16',
                                        weight: 'light',
                                        alight: 'right'
                                    },
                                    content: '+7 (926) 396-03-66'
                                },
                                {
                                    block: 'link',
                                    url: '#',
                                    mix:{
                                        block: 'text',
                                        mods:{
                                            size: '16',
                                            weight: 'light',
                                            alight: 'right',
                                            view: 'link'
                                        }
                                    },
                                    content: 'Записатсья'
                                },
                            ]
                        }
                    ]
                }
            ]
        },
        
    ]
});
