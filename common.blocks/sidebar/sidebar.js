modules.define('sidebar', ['i-bem-dom'], function(provide, bemDom) {

    provide(bemDom.declBlock(this.name, {
        onSetMod: {
            js: {
                inited: function() {

                    $('.sidebar__link').click(function() {
                        var iselemnt = $(this).next();
                        var main = $(this);
                        if((iselemnt.is('.sidebar__level')) && (iselemnt.is(':visible'))) {
                            main.removeClass('active');
                            iselemnt.slideUp('normal');

                            console.log('close');
                            return false;
                        }
                    
                        if((iselemnt.is('.sidebar__level')) && (!iselemnt.is(':visible'))) {


                            $('.sidebar__link').removeClass('active');
                            main.addClass('active');

                            $('.sidebar__level:visible').slideUp('normal');
                            iselemnt.slideDown('normal');
                            
                            console.log('open');
                            return false;
                        }
                    });
                }
            }
        }
    }));
});
