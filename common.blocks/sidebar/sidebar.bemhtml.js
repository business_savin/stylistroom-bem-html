block('sidebar')(
    content()(function () {
        return this.ctx.sidebar.map(function (item) {
            return [
                {
                    elem: 'item',
                    content: [

                        {
                            block: 'link',
                            mix: [
                                { block: 'sidebar', elem: 'link' },
                                { block: 'sidebar', elem: 'link-main' },
                                {
                                    block: 'text',
                                }
                            ],
                            tag: 'a',
                            attrs: {
                                href: item.url,
                                title: item.name
                            },
                            content: [
                                {
                                    tag: 'span',
                                    content: item.name
                                },
                                item.level && {
                                    block: 'icon',
                                    mods: {
                                        type: 'arrow'
                                    },
                                    tag: 'span'
                                },


                            ]
                        },
                        item.level && [
                            {
                                block: 'sidebar',
                                elem: 'level',
                                mix: {
                                    block: 'sidebar',
                                    elem: 'indent'
                                },
                                tag: 'ul',
                                content: item.level
                            }
                        ],
                    ]
                },

            ];

        });

    }),

    elem('list')(
        tag()('ul')
    ),
    elem('link')(
        tag()('a')
    ),
    elem('item')(
        tag()('li')
    )
);
block('sidebar').content()(function () {
    return {
        elem: 'list',
        content: applyNext()

    };
});
block("sidebar").elem('level')(
    content()(function (node, { content }) {
        return content.map(function (item) {
            return [
                {
                    block: 'sidebar',
                    elem: 'item',
                    tag: 'li',
                    content: [
                        {
                            block: 'text',
                            mix: [
                                {
                                    block: 'sidebar',
                                    elem: 'link'
                                },
                                {
                                    block: 'link'
                                },
                                {
                                    block: 'sidebar',
                                    elem: 'link-h2'
                                },
                                {
                                    block: 'tabs',
                                    elem: 'tab'
                                }
                            ],
                            attrs: {
                                href: item.url
                            },
                            tag: 'a',
                            content: item.name
                        },
                        item.level && [
                            {
                                block: 'sidebar',
                                elem: 'level',
                                mix: [
                                    {
                                        block: 'sidebar',
                                        elem: 'indent'
                                    }
                                ],
                                tag: 'ul',
                                content: item.level
                            }
                        ],

                    ]
                }
            ]
        })
    })
);